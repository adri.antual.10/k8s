import http from "k6/http";
import {URL} from 'https://jslib.k6.io/url/1.0.0/index.js';
import { check, sleep } from "k6";

// Test configuration
export const options = {
    insecureSkipTLSVerify: true,
    scenarios: {
        constant_request_rate: {
            executor: 'constant-arrival-rate',
            rate: 1000, //  RPS
            timeUnit: '1s', // N iterations per second, i.e. N RPS
            duration: '30s',
            preAllocatedVUs: 1000, // how large the initial pool of VUs would be
            maxVUs: 1000, // if the preAllocatedVUs are not enough, we can initialize more
        },
    },
};

export default function () {
    const url = new URL('https://k8s-lia.unrn.edu.ar/prueba01');

    const resp = http.get(url.toString());

    console.log(resp.body);

    // Validate response status
    check(resp, { "nxginx status was 200" : (r) => {console.log(r.status); return r.status === 200} });
}